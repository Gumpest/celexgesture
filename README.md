
## CeleXGesture

The dynamic vision gesture dataset named CeleXGesture is made by *PKU Intelligent hardware and system LAB* and saved in .bin format for convenience.  The dataset consists of 2000 instances of a set of 10 gestures  *(hand clap, rock, right arm clockwise, right arm counter  clockwise, left arm clockwise, left arm counter clockwise, arm  roll, right arm updown and left arm updown)* with time sequence. 

The CeleXGesture is under three different lighting  conditions *(natural light, fluorescent light and LED light)* at  four different distances *(1.5m, 1.6m, 1.7m and 1.8m)* to  improve compatibility on applications. The six presenters *(five men and a woman)* change their position and the movement range  indefinitely for data augmentation. Each gesture lasts about 3 seconds. 

The spike events are represented as (x, y, p, t), which  are a sequence of spikes suitable well as SNN neuromorphic  computing. 

These gestures are captured  by a **CeleX-V MP** camera. 

The demo file lists `.mkv` videos and `.csv` files of each gesture. You can get both beyond formats of data events from converting `.bin` files with [SDK](https://github.com/CelePixel/CeleX5-MIPI) for CeleX5 sensor.



